﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    private string name;
    private float weight;
    private Sprite sprite;

    public Item(string name, Sprite sprite, float weight)
    {
        this.name = name;
        this.weight = weight;
        this.sprite = sprite;
    }

    public Sprite GetSprite()
    {
        return sprite;
    }

    public virtual void EnchantItem(int power)
    {

    }
}
