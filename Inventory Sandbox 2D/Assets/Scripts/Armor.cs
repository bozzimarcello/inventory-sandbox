﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Item
{
    private float defence;
    public Armor(string name, Sprite sprite, float weight, float defence)
        : base(name, sprite, weight)
    {
        this.defence = defence;
    }

    public float GetProtection()
    {
        return defence;
    }

    public void SetProtection(int defence)
    {
        this.defence = defence;
    }
    public void SetProtection(float defence)
    {
        this.defence = defence;
    }

    public override void EnchantItem(int power)
    {
        defence += power;
    }
}
