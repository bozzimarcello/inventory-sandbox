﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    [SerializeField] GameObject itemPrefab;
    [SerializeField] List<Sprite> sprites;

    private Inventory inventory;
    private float topLeftCornerX = 2f;
    private float topLeftCornerY = 6f;
    private float offset = 3f;

    void Start()
    {
        inventory = new Inventory();
        
        Item apple = new Item("apple", sprites[0], 1f);
        inventory.AddItem(apple);

        Armor helmets = new Armor("helmets", sprites[1], 3f, 15f);
        inventory.AddItem(helmets);

        Weapon axe = new Weapon("axe", sprites[2], 8f, 25f);
        inventory.AddItem(axe);

        Item book = new Item("book", sprites[3], 1f);
        inventory.AddItem(book);

        Armor armor = new Armor("armor", sprites[4], 10f, 65f);
        inventory.AddItem(armor);

        Item gem = new Item("gem", sprites[5], 0.5f);
        inventory.AddItem(gem);

        Weapon sword = new Weapon("sword", sprites[6], 6f, 15f);
        inventory.AddItem(sword);
    }

    public void ShowArmors()
    {
        HideItems();
        float offsetX = 0f;
        foreach (Item i in inventory.GetArmors())
        {
            IstantiateItem(i, offsetX);
            offsetX+= offset;
        }
    }

    public void ShowWeapons()
    {
        HideItems();
        float offsetX = 0f;
        foreach (Item i in inventory.GetWeapons())
        {
            IstantiateItem(i, offsetX);
            offsetX += offset;
        }
    }
    public void ShowItems()
    {
        HideItems();
        float offsetX = 0f;
        foreach (Item i in inventory.GetItems())
        {
            IstantiateItem(i, offsetX);
            offsetX += offset;
        }
    }

    private void IstantiateItem(Item i, float offset)
    {
        GameObject item = Instantiate(itemPrefab, 
            new Vector3(topLeftCornerX + offset, topLeftCornerY, 0), 
            Quaternion.identity);
        item.GetComponent<SpriteRenderer>().sprite = i.GetSprite();
    }

    public void HideItems()
    {
        foreach(GameObject o in GameObject.FindGameObjectsWithTag("inventoryItem"))
        {
            Destroy(o);
        } 
    }

}
