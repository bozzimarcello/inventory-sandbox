﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item
{
    private float damage;

    public Weapon(string name, Sprite sprite, float weight, float damage) 
        : base(name, sprite, weight)
    {
        this.damage = damage;
    }

    public override void EnchantItem(int power)
    {
        damage += power;
    }
}
