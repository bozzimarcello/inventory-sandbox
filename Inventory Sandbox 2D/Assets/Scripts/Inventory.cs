﻿using System.Collections;
using System.Collections.Generic;

public class Inventory
{
    private List<Item> items;

    public Inventory()
    {
        items = new List<Item>();
    }

    public void AddItem(Item item)
    {
        items.Add(item);
    }

    public List<Item> GetAllItems()
    {
        return items;
    }
    public List<Item> GetItems()
    {
        List<Item> otherItems = new List<Item>();

        foreach (Item i in items)
        {
            if (!(i is Weapon) && !(i is Armor))
            {
                otherItems.Add(i);
            }
        }
        return otherItems;
    }
    public List<Item> GetWeapons()
    {
        List<Item> weapons = new List<Item>();

        foreach (Item i in items)
        {
            if (i is Weapon)
            {
                weapons.Add(i);
            }
        }
        return weapons;
    }

    public List<Item> GetArmors()
    {
        List<Item> armors = new List<Item>();

        foreach (Item i in items)
        {
            if (i is Armor)
            {
                armors.Add(i);
            }
        }
        return armors;
    }

}
